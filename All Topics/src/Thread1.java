import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Thread1 implements ActionListener {

	Container container;
	JFrame frame = new JFrame();
	JLabel displayLabel = new JLabel(" ");
	Display display = null;
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JButton stop = new JButton("STOP");
	JButton restart = new JButton("Restart");

	public Thread1() {
		container = frame.getContentPane();
		frame.setSize(100, 150);
		frame.setVisible(true);
		container.setLayout(new FlowLayout());
		p1.setLayout(new GridLayout(1, 2));
		p2.setLayout(new GridLayout(1, 2));

		JLabel label = new JLabel("Time :");
		p1.add(label);
		p1.add(displayLabel);
		p2.add(stop);
		p2.add(restart);
		container.add(p1);
		container.add(p2);
		display = new Display(displayLabel);
		display.start();

	}

	public static void main(String[] args) {
		new Thread1();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object target = e.getSource();

		if (target == stop) {
			display.mySuspend();
		}
		if (target == restart) {
			display.myResume();
		}

	}

}

class Display extends Thread {
	private boolean flag = false;
	private JLabel label ;

	public Display(JLabel l) {
		label = l;
	}

	public void mySuspend() {
		flag = true;
	}

	public void myResume() {
		flag = false;
		notify();
	}

	public void format() {
		Date now = new Date();
		SimpleDateFormat d = new SimpleDateFormat("HH:mm:ss");
		label.setText(d.format(now));
	}

	public void run() {
		while (true) {
			format();
			try {
				Thread.sleep(1000);
				synchronized (this) {
					while (flag) {
						wait();
					}
				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}
	}

}
