import java.util.ArrayList;
import java.util.List;

//
public class Test11 {
	
	public static ArrayList<String> list=new ArrayList<String>();
	public static void permute(String str,int l,int r) {
		if (l == r)
	       {
	       list.add(str); // add permutation in the list
	       //System.out.println(str);
	       }
	       else
	       {
	           for (int i = l; i <= r; i++)
	           {
	               str = swap(str,l,i);
	               permute(str, l+1, r);
	               str = swap(str,l,i);
	           }
	       }
	}
	public static String swap(String a, int i, int j)
	   {
	       char temp;
	       char[] charArray = a.toCharArray();
	       temp = charArray[i] ;
	       charArray[i] = charArray[j];
	       charArray[j] = temp;
	       return String.valueOf(charArray);
	   }
	
	public static String solution(int a,int b,int c,int d) {
		list.clear();
		String no=""+a+b+c+d;
		String ans="";
		int maxMinutes=-1;
		permute(no,0,3);
		for(String str:list) {
			int aa= Character.getNumericValue(str.charAt(0));
			int bb= Character.getNumericValue(str.charAt(1));
			int cc= Character.getNumericValue(str.charAt(2));
			int dd= Character.getNumericValue(str.charAt(3));
			
			
			int hour= aa*10+bb;
			int minute=cc*10+dd;
			
			if(hour<=23 && minute<=59) {
				int totMinutes= hour*60+minute;
				if(totMinutes>maxMinutes) {
					maxMinutes=totMinutes;
					ans= aa+""+bb+":"+cc+""+dd;
				}
				
			}
			System.out.println(maxMinutes);
			
		}
		if(maxMinutes==-1) {
			ans="Not Possible";
		}
		
		return ans;
	}
	
public static void main(String[]args) {
	
	String s= "2345";
	String ss= solution(2,2,3,4);
	System.out.println(ss);

	
	
	

}
}
