import java.util.Arrays;

public class CodilityT1 {

	public static int binary(int N) {

		int binaryGap = 0;

		String s = Integer.toBinaryString(N);

		char[] c = s.toCharArray();

		Character cc;

		int j = 0;

		for (int i = 0; i < c.length; i++) {

			cc = c[i];
			System.out.print(cc);

			if (cc.equals('0')) {
				j++;
			}

			if (cc.equals('1')) {
				if (j > binaryGap) {
					binaryGap = j;
				}
				j = 0;

			}
		}

		return binaryGap;

	}

	public static String maxAmp(int[] A) {

		int n = A.length;
		int k = n / 4;
		int index = 0;
		int max = A[0];

		// for(int i=0;i<n;i++) {
		// if(A[i]>max) {
		// max=A[i];
		// index=i;
		// }
		//
		// }

		int[] winter = new int[k];
		int[] spring = new int[k];
		int[] summer = new int[k];
		int[] autumn = new int[k];

		int[] maxAmp = new int[4];

		int i = 0;

		// winter=Arrays.copyOfRange(A, i, i+k);
		// spring=Arrays.copyOfRange(A, i+k, i+k*2);
		// summer= Arrays.copyOfRange(A, i+k*2, i+k*3);
		// autumn =Arrays.copyOfRange(A, i*3, i+k*4);
		//
		//
		// Arrays.sort(winter);
		// Arrays.sort(spring);
		// Arrays.sort(summer);
		// Arrays.sort(autumn);
		//
		int winterDiff = 0;
		int springDiff = 0;
		int summerDiff = 0;
		int autumnDiff = 0;
		//
		// for(int j:winter) {
		// winterDiff= winter[k-1]-winter[k-2];
		// }
		// for(int j:spring) {
		// springDiff= spring[k-1]-spring[k-2];
		// }
		// for(int j:summer) {
		// System.out.println(j);
		// }for(int j:autumn) {
		// System.out.println(j);
		// }

		for (int j : A) {
			winterDiff = A[k - 1] - A[k - 2];
			springDiff = A[k * 2 - 1] - A[k * 2 - 2];
			summerDiff = A[k * 3 - 1] - A[k * 3 - 2];
			autumnDiff = A[k * 4 - 1] - A[k * 4 - 2];

		}

		if (winterDiff > springDiff && winterDiff > summerDiff && winterDiff > autumnDiff) {
			return "winter";
		} else if (autumnDiff > winterDiff && autumnDiff > summerDiff && autumnDiff > springDiff) {
			return "autumn";
		}

		else if (springDiff > winterDiff && springDiff > summerDiff && springDiff > autumnDiff) {
			return "spring";
		} else {
			return "summer";
		}

	}

	public static int chessPoints(String[] B) {

		int points = 0;

		int posO = 0;
		int posX = 0;

		String s = Arrays.toString(B).replace(",", "").replace(" ", "").replace("[", "").replace("]", "");
		// char[]c= s.toCharArray();

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == 'O') {
				posO = i;
			}

		}

		System.out.print(s.length());
		
		for (int i = 1; i <= s.length(); i++) {
			
			if (s.charAt((posO - 7)) == 'X' && s.charAt(posO - 14) == 'X') {
				points=0;
			} else if (s.charAt((posO - 5)) == 'X' && s.charAt(posO - 10) == 'X') {

				points=0;

			} else if (s.charAt((posO - 5 )) == 'X' && s.charAt(posO - 10) == '.') {

				posO = posO - 10;
				points++;
			} else if (s.charAt((posO - 7)) == 'X' && s.charAt(posO - 14) == '.') {

				posO = posO - 14;
				points++;
			}
		}

//		if (posX - 1 < 0) {
//			return 0;
//		}

		return points;
	}

	public static void main(String[] args) {

		// int binaryNo = binary(5);

		String[] B = { "..X...", "......", "....X.", ".X....", "..X.X.", "...O.." };

		int points = chessPoints(B);
		// System.out.println(points);
		// System.out.println(binaryNo);
		int[] amp = { 2, 6, 30, 30, 81, 87, 9, 11 };
		String amp1 = maxAmp(amp);
		 System.out.println(points);

	}

}
