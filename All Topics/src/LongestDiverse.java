
public class LongestDiverse {
	static void solution(int a, int b, int c) {
		int n = 3;
		// hold data in array for sorting purpose
		int count[] = { a, b, c };

		// also stored annotation
		char notation[] = { 'a', 'b', 'c' };
		// used selection sort to sort count and annotation
		// sort elements in descresing order-
		// example a-4, b-3, c-9
		// output c-9 a-4 b-3
		for (int i = 0; i < n - 1; i++) {
			// Find the minimum element in unsorted array
			int min_idx = i;
			for (int j = i + 1; j < n; j++)
				if (count[j] > count[min_idx])
					min_idx = j;

			int temp = count[min_idx];
			char tempchar = notation[min_idx];
			count[min_idx] = count[i];
			notation[min_idx] = notation[i];
			count[i] = temp;
			notation[i] = tempchar;
		}
		// stringbuilder to hold final result
		StringBuilder builder = new StringBuilder();
		// finally got sorted array and print it on terminal
		System.out.println("sorted Annotation-count are " + notation[0] + "-" + count[0] + "," + notation[1] + "-"
				+ count[1] + "," + notation[2] + "-" + count[2]);
		// travel till count of 1st element(largest count) reaches to zero but keep 2nd
		// or 3rd element non empty
		while (count[0] > 0 && (count[1] != 0 || count[2] != 0)) {
			if (count[0] >= 2) { // if count of 1st element is greater than 2
				builder.append(notation[0]); // append 2 time 1st annotation
				builder.append(notation[0]);

				count[0] = count[0] - 2;// decrease count by 2
				if (count[0] >= 6) {
					count[0] = count[0] + 2;
				}
			}

			else if (count[0] == 1) { // if count of 1st element is one
				builder.append(notation[0]);// append one time 1st annotation
				count[0] = count[0] - 1;// decrease count by 1
			}

			// similarly for 2nd elemenents
			if (count[1] >= 2) {
				builder.append(notation[1]);
				builder.append(notation[1]);

				count[1] = count[1] - 2;
			} else if (count[1] == 1) {
				builder.append(notation[1]);
				count[1] = count[1] - 1;
			}
			// similarly for 3rd elemenents
			if (count[2] >= 2) {
				builder.append(notation[2]);
				builder.append(notation[2]);
				count[2] = count[2] - 2;
			} else if (count[2] == 1) {
				builder.append(notation[2]);
				count[2] = count[2] - 1;
			}

		}
		// when while loop goes to end then finally add 1st elements if any remaininig
		if (count[0] >= 2) {
			builder.append(notation[0]);
			builder.append(notation[0]);
			count[0] = count[0] - 2;
		} else if (count[0] == 1) {
			builder.append(notation[0]);
			count[0] = count[0] - 1;
		}
		int countA = 0;
		int countB = 0;
		int countC = 0;

		int missingA = 0;
		int missingB = 0;
		int missingC = 0;

		String ans = builder.toString();

		char[] cc = ans.toCharArray();

		for (int i = 0; i < ans.length(); i++) {
			if (cc[i] == 'a') {
				countA++;
			} else if (cc[i] == 'b') {
				countB++;
			} else if (cc[i] == 'c') {
				countC++;
			}
		}

		missingA = a - countA;
		missingB = b - countB;
		missingC = c - countC;
		System.out.println(missingB);

		if (missingA > 0 && a > 0 && b > 0 && c > 0) {

			String ch = "";

			for (int i = 0; i < missingA; i++) {
				ch = ch + "a";
			}
			ans = (ans.substring(0, 3) + ch + ans.substring(3, ans.length()));

			System.out.println(ans);

			// ans= (ans.substring(2,3)+r.toString()+ans.substring(a+b+c));

		} else if (missingB > 0 && a > 0 && b > 0 && c > 0) {
			String ch = "";

			for (int i = 0; i < missingB; i++) {
				ch = ch + "b";
			}
			ans = (ans.substring(0, 3) + ch + ans.substring(3, ans.length()));

			System.out.println(ans);
		} else if (missingC > 0 && a > 0 && b > 0 && c > 0) {
			String ch = "";

			for (int i = 0; i < missingC; i++) {
				ch = ch + "c";
			}
			ans = (ans.substring(0, 3) + ch + ans.substring(3, ans.length()));
		}

		System.out.println("Output::" + ans);

		// System.out.println("Output::"+countC);

	}

	public static void main(String[] args) {
		solution(3, 6, 5);
	}

}
