
public class CodilityGuideWire {

	public static int solution(int N) {

		int number = N;
		if (N < 0) {
			number = N * -1;
		} else {
			number = N;
		}

		String s = String.valueOf(number);

		int max = -8000;

		int[] arr = new int[s.length() + 1];

		for (int i = 0; i < arr.length - 1; i++) {
			arr[i] = Integer.parseInt(s.substring(0, i) + "5" + s.substring(i));
		}
		arr[s.length()] = Integer.parseInt(s + "5");

		for (int i = 0; i < arr.length - 1; i++) {
			if (N < 0) {
				arr[i] = arr[i] * -1;
			}
			if (arr[i] > max) {
				max = arr[i];
			}

		}

		return max;
	}

	public static void main(String[] args) {

		int max = solution(-999);
		System.out.println(max);
	}

}
